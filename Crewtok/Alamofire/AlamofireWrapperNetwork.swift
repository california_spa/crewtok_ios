//
//  AlamofireWrapperNetwork.swift
//  WheyPoint
//
//  Created by Rishabh Arora on 10/31/18.
//  Copyright © 2017 Rishabh Arora. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

typealias JSONDictionary = JSON
typealias JSONArray = [JSON]
typealias APIServiceFailureCallback = ((Error?) -> ())
typealias JSONArrayResponseCallback = ((JSONArray?,Data?) -> ())
typealias JSONDictionaryResponseCallback = ((JSONDictionary?,Data?) -> ())


class AlamoFireWrapperNetwork{
    
    class var sharedInstance: AlamoFireWrapperNetwork{
        struct Singleton{
            static let instance = AlamoFireWrapperNetwork()
        }
        return Singleton.instance
    }
    private init(){}
    
    
    //MARK:- Get Categories List Function
    func GetDataAlamofire(url:String, headers:[String:String]? = nil, isHide:Bool = true,success: @escaping JSONDictionaryResponseCallback, failure: @escaping APIServiceFailureCallback) {
        
        Alamofire.request(BASE_URL+"\(url)", method: .get, parameters: nil, headers: nil).responseJSON {
            (response:DataResponse<Any>) in
            if isHide{
            applicationDelegate.hideActivityIndicator()
            }
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    if let responseDict = response.result.value {
                        if response.response?.statusCode == 401 {
                            self.goToInitialVc()
                        }
                        else{
                            success(JSON(responseDict),response.data)
                        }
                    }
                }
                break
            case .failure(_):
                failure(response.result.error)
                break
            }
        }
    }
    
    func PostDataAlamofire(_ parameters:[String : Any]? = nil,url:String, headers:[String:String]? = nil,success:  @escaping JSONDictionaryResponseCallback, failure: @escaping APIServiceFailureCallback) {
        Alamofire.request(BASE_URL+"\(url)", method: .post, parameters: parameters, headers: headers).responseJSON {
            (response:DataResponse<Any>) in
            applicationDelegate.hideActivityIndicator()
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    if let responseDict = response.result.value {
                        if response.response?.statusCode == 401 {
                            self.goToInitialVc()
                        }
                        else{
                            success(JSON(responseDict),response.data)
                        }
                    }
                }
                break
            case .failure(_):
                failure(response.result.error)
                break
            }
        }
    }
    
    //MARK:- Post Multipart Data Function
    
    func PostMultipartData(_ parameters:[String : String]? = nil,url:String, headers:[String:String]? = nil, imageSingle:UIImage? = nil,imgFileParam:String? = "image", document:Data? = nil,docExt:String = "jpeg",success:  @escaping JSONDictionaryResponseCallback, failure: @escaping APIServiceFailureCallback) {
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            let imageName = Date().timeIntervalSince1970
            if let myImage = imageSingle{
                let imageData = myImage.pngData()
                multipartFormData.append(imageData!, withName: imgFileParam!,fileName: "\(imageName).\(docExt)", mimeType: "image/jpeg")
            }
            
            if let doc = document{
                multipartFormData.append(doc, withName: imgFileParam!,fileName: "\(imageName).\(docExt)", mimeType: "")//application/octet-stream
            }
            
            if parameters != nil{
                for (key, value) in parameters! {
                    multipartFormData.append((value).data(using: String.Encoding.utf8)!, withName: key)
                }
            }
        },to:BASE_URL+"\(url)", headers: headers)
        { (result) in
           
            switch(result) {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    debugPrint(progress)
                })
                
                upload.responseJSON { response in
                     applicationDelegate.hideActivityIndicator()
                    if let responseDict = response.result.value {
                        if response.response?.statusCode == 401 {
                            self.goToInitialVc()
                        }
                        else{
                            success(JSON(responseDict),response.data)
                        }
                    }
                }
                break
            case .failure(_):
                break
            }
        }
    }
    
    func PostMultipartDataMultiple(_ parameters:[String : String]? = nil,url:String, headers:[String:String]? = nil, imageArray:NSMutableArray,success:  @escaping JSONDictionaryResponseCallback, failure: @escaping APIServiceFailureCallback) {
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            if imageArray.count>0{
                for (index,obj) in imageArray.enumerated(){
                    let imageName = Date().timeIntervalSince1970
                    let myImage = obj as? UIImage
                    let imageData = myImage!.pngData()
                    let productNo = index + 1
                    if let unwrappedData = imageData{
                        multipartFormData.append(unwrappedData, withName: "product_image[\(productNo)]",fileName: "\(imageName).png", mimeType: "image/jpeg")
                    }}
            }
            if parameters != nil{
                for (key, value) in parameters! {
                    multipartFormData.append((value).data(using: String.Encoding.utf8)!, withName: key)
                }
            }
        },to:BASE_URL+"\(url)")
        { (result) in
            applicationDelegate.hideActivityIndicator()
            switch(result) {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    debugPrint(progress)
                })
                
                upload.responseJSON { response in
                    if let responseDict = response.result.value {
                        if response.response?.statusCode == 401 {
                            self.goToInitialVc()
                        }
                        else{
                            success(JSON(responseDict),response.data)
                        }
                    }
                }
                break
            case .failure(_):
                break
            }
        }
    }
    
    func goToInitialVc(){
        loginStatus = false
        let initialVC = mainStoryboard.instantiateViewController(withIdentifier:"LoginVC") as! LoginVC
        let destinationController = UINavigationController()
        destinationController.viewControllers = [initialVC]
        destinationController.isNavigationBarHidden = true
        applicationDelegate.window?.rootViewController = destinationController
    }
    
}
