//
//  AppDelegate.swift
//  CrewtokApp
//
//  Created by MAC on 21/01/19.
//  Copyright © 2019 Rishabh Arora. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
//import GoogleMaps
import UserNotifications
//import Firebase
import SwiftMessageBar
import LGSideMenuController
import GooglePlaces
import CoreLocation
import SVProgressHUD

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var deviceTokenString = String()
    var isTimeClock = false
    var lastClockedMessage = String()
    var assignjobs = String()
    var locationManager = CLLocationManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        
        GMSPlacesClient.provideAPIKey("AIzaSyDV52o5QOKjpj3TwhKHlmaYa0IFt9B4zmU")
        //        GMSServices.provideAPIKey("AIzaSyDV52o5QOKjpj3TwhKHlmaYa0IFt9B4zmU")
        
        //        FirebaseApp.configure()
        //        Messaging.messaging().delegate = self
        //
        //       // Register for Push Notifications
        //        UNUserNotificationCenter.current().delegate = self
        //        registerForPushNotifications()
        
        getLocation()
        
        if loginStatus ?? false{
            if loginData != nil{
                let jsonDecoder = JSONDecoder()
                profileData = try! jsonDecoder.decode(ProfileModel.self, from: loginData!)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                let navController = UINavigationController(rootViewController: vc)
                navController.isNavigationBarHidden = true
                
                let lgController = mainStoryboard.instantiateViewController(withIdentifier: "LGSideMenuController") as! LGSideMenuController
                if UIDevice.isPad{
                    lgController.leftViewWidth = 300
                }
                else{
                    lgController.leftViewWidth = 260
                }
                lgController.rootViewController = navController
                self.window?.rootViewController = lgController
            }
        }
        
        return true
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK:- RegisterForPushNotifications
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge,.carPlay]) {
            (granted, error) in
            guard granted else { return }
            
            self.getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    
    //        //Push Notifications Methods:-
    //    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
    //        print("Firebase registration token: \(fcmToken)")
    //        self.deviceTokenString = fcmToken
    //    }
    
    
    //    func application(_ application: UIApplication,
    //                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    //        let tokenParts = deviceToken.map { data -> String in
    //            return String(format: "%02.2hhx", data)
    //        }
    //        let token = tokenParts.joined()
    //
    //        debugPrint("Device Token: \(token)")
    //        self.deviceToken = token
    //    }
    //
    //    func application(_ application: UIApplication,
    //                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
    //        print("Failed to register: \(error)")
    //    }
    //
    //    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
    //
    //        guard let dialogID = userInfo["dialog_id"] as? String else {
    //            guard let messageType = ((userInfo.validatedValue("aps", expected: [:] as AnyObject) as! Dictionary<String, AnyObject>).validatedValue("alert", expected: [:] as AnyObject) as! Dictionary<String, AnyObject>).validatedValue("event_type", expected: "" as AnyObject) as? String else {
    //                return
    //            }
    //
    //            return
    //        }
    //
    //        guard !dialogID.isEmpty else {
    //            return
    //        }
    //
    //    }
    //
    //
    //    @available(iOS 10.0, *)
    //    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    //        let userInfo = notification.request.content.userInfo
    //        handleNotifications(userInfo: userInfo)
    //        completionHandler([.alert,.badge,.sound])
    //    }
    //
    //    @available(iOS 10.0, *)
    //    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    //        let userInfo = response.notification.request.content.userInfo
    //        handleNotifications(userInfo: userInfo)
    //        completionHandler()
    //    }
    
    //MARK:- activityIndicator
    func showActivityIndicator()
    {
        UIApplication.shared.beginIgnoringInteractionEvents()
        SVProgressHUD.show()
    }
    
    //MARK:- hideActivityIndicator
    func hideActivityIndicator()
    {
        UIApplication.shared.endIgnoringInteractionEvents()
        SVProgressHUD.dismiss()
    }
    
    
}


extension AppDelegate:CLLocationManagerDelegate{
    
    func getLocation(){
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.allowsBackgroundLocationUpdates = true
            locationManager.pausesLocationUpdatesAutomatically = false
            locationManager.startUpdatingLocation()
        }
    }
    
    //Check authorization
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let location = locations.last! as CLLocation
        sourceLat = location.coordinate.latitude
        sourceLong = location.coordinate.longitude
        
        getAddressFromLocation(location)
        
        locationManager.stopUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        
    }
    
    func getAddressFromLocation(_ loc:CLLocation){
        
        let ceo: CLGeocoder = CLGeocoder()
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country)
                    print(pm.locality)
                    print(pm.subLocality)
                    print(pm.thoroughfare)
                    print(pm.postalCode)
                    print(pm.subThoroughfare)
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    print(addressString)
                    sourceLoc = addressString
                    if loginData != nil{
                        self.apiUpdateLocation(addressString)
                    }
                    
                }
        })
        
    }
    
    func apiUpdateLocation(_ address:String){
        if (reachability?.isReachable)! {
            applicationDelegate.showActivityIndicator()
            
            let headerDict = ["Accept":"application/json"]
            let parameterDict = ["userid":profileData?.sessionid ?? "","location":address]
            
            AlamoFireWrapperNetwork.sharedInstance.PostDataAlamofire(parameterDict as [String : Any],url: Api.UPDATE_LIVE_STATUS.rawValue,headers: headerDict, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
    func showAlert(message :String,type: MessageType){
        DispatchQueue.main.async {
            let messageBarConfig = SwiftMessageBar.Config(successColor: UIColor(displayP3Red: 0/255, green: 123/255, blue: 70/255, alpha: 1.0), isStatusBarHidden: true)
            SwiftMessageBar.setSharedConfig(messageBarConfig)
            SwiftMessageBar.showMessage(withTitle: appName, message: message, type: type, duration: 2) {
                print("Dismiss callback")
            }
            
        }
    }
    
}
