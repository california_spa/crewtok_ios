//
//  BaseViewController.swift
//  Stadfalke Vendor
//
//  Created by Rishabh Arora on 2/13/18.
//  Copyright © 2018 Rishabh Arora. All rights reserved.
//

import UIKit
import SwiftMessageBar
import LGSideMenuController

class BaseViewController:UIViewController{
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        debugPrint("MEMORY WARNING")
    }
    
    
    // MARK:- Common Actions
    
    func backBtnAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func dismissBtnAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func removeFromParentBtnAction(){
        self.willMove(toParent: nil)
        self.removeFromParent()
        self.view.removeFromSuperview()
    }
    
    //MARK: Tabbar controller methods
    
    func hideTabbar() {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func showTabbar() {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func goToProfileDetailVc(){
        loginStatus = true
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ProfileDetailVC") as! ProfileDetailVC
        let navController = UINavigationController(rootViewController: vc)
        navController.isNavigationBarHidden = true
        
        let lgController = mainStoryboard.instantiateViewController(withIdentifier: "LGSideMenuController") as! LGSideMenuController
        if UIDevice.isPad{
            lgController.leftViewWidth = 300
        }
        else{
            lgController.leftViewWidth = 260
        }
        lgController.rootViewController = navController
        applicationDelegate.window?.rootViewController = lgController
    }
    
    func goToHomeVc(){
        loginStatus = true
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let navController = UINavigationController(rootViewController: vc)
        navController.isNavigationBarHidden = true
        
        let lgController = mainStoryboard.instantiateViewController(withIdentifier: "LGSideMenuController") as! LGSideMenuController
        if UIDevice.isPad{
            lgController.leftViewWidth = 300
        }
        else{
            lgController.leftViewWidth = 260
        }
        lgController.rootViewController = navController
        applicationDelegate.window?.rootViewController = lgController
    }
    
    func goToAssignedVc(){
        let vc = mainStoryboard.instantiateViewController(withIdentifier:"AssignedVc") as! AssignedVc
        let navController = UINavigationController(rootViewController: vc)
        navController.isNavigationBarHidden = true
        
        let lgController = mainStoryboard.instantiateViewController(withIdentifier: "LGSideMenuController") as! LGSideMenuController
        if UIDevice.isPad{
            lgController.leftViewWidth = 300
        }
        else{
            lgController.leftViewWidth = 260
        }
        lgController.rootViewController = navController
        applicationDelegate.window?.rootViewController = lgController
    }
    
    func goToContactVC(){
        let vc = mainStoryboard.instantiateViewController(withIdentifier:"ContactVC") as! ContactVC
        let navController = UINavigationController(rootViewController: vc)
        navController.isNavigationBarHidden = true
        
        let lgController = mainStoryboard.instantiateViewController(withIdentifier: "LGSideMenuController") as! LGSideMenuController
        if UIDevice.isPad{
            lgController.leftViewWidth = 300
        }
        else{
            lgController.leftViewWidth = 260
        }
        lgController.rootViewController = navController
        applicationDelegate.window?.rootViewController = lgController
    }
    
    func goToAssigmentVc(){
        let vc = mainStoryboard.instantiateViewController(withIdentifier:"AssigmentVc") as! AssigmentVc
        let navController = UINavigationController(rootViewController: vc)
        navController.isNavigationBarHidden = true
        
        let lgController = mainStoryboard.instantiateViewController(withIdentifier: "LGSideMenuController") as! LGSideMenuController
        if UIDevice.isPad{
            lgController.leftViewWidth = 300
        }
        else{
            lgController.leftViewWidth = 260
        }
        lgController.rootViewController = navController
        applicationDelegate.window?.rootViewController = lgController
    }
    
    
    func goToInitialVc(){
        loginStatus = false
        loginData = nil
        let vc = mainStoryboard.instantiateViewController(withIdentifier:"welcomeVc") as! welcomeVc
        let destinationController = UINavigationController.init(rootViewController: vc)
        destinationController.isNavigationBarHidden = true
        applicationDelegate.window?.rootViewController = destinationController
    }
    
    func getCurrentDateTime(_ dateFormat:String = "MM/dd/yyyy", timeFormat:String = "hh:mm a") -> (String,String){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        let dateStr = dateFormatter.string(from: Date())
        dateFormatter.dateFormat = timeFormat
        let timeStr = dateFormatter.string(from: Date())
        return (dateStr,timeStr)
    }
    
    func TimeStampToString(_ stamp:String,format:String = "MM/dd/yyyy")->String {
        if stamp != ""{
            let date = Date(timeIntervalSince1970: Double(stamp)!)
            let dateFormatter = DateFormatter()
            dateFormatter.locale = NSLocale.current
            dateFormatter.dateFormat = format //Specify your format that you want
            let strDate:String = dateFormatter.string(from: date)
            return strDate
        }
        else{
            return "Invalid date"
        }
    }
    
}

//MARK: Alert View Controller Delegates
extension BaseViewController {
    
    //MARK: - Alert/Error/Notification Implementation
    
    func showCustomAlert(message: String?, title:String? = appName , otherButtons:[String:((UIAlertAction)-> ())]? = nil, cancelTitle: String = kOk, cancelAction: ((UIAlertAction)-> ())? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelAction))
        
        if otherButtons != nil {
            for key in otherButtons!.keys {
                alert.addAction(UIAlertAction(title: key, style: .default, handler: otherButtons![key]))
            }
        }
        present(alert, animated: true, completion: nil)
    }
    
    //MARK: Show toast on click
    func showToastWithMessage(message: String) {
        let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    func showAlert(message :String,type: MessageType){
        DispatchQueue.main.async {
            let messageBarConfig = SwiftMessageBar.Config(successColor: UIColor(displayP3Red: 0/255, green: 123/255, blue: 70/255, alpha: 1.0), isStatusBarHidden: true)
            SwiftMessageBar.setSharedConfig(messageBarConfig)
            SwiftMessageBar.showMessage(withTitle: appName, message: message, type: type, duration: 2) {
                print("Dismiss callback")
            }
            
        }
    }
    
    //MARK:- Open Url
    
    func openUrl(_ url:String){
        guard let url = URL(string: url) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    //Mark:- CornerRadius
    
    func cornerRadius(vw:[UIView]){
        for subVw in vw{
            subVw.layer.cornerRadius = subVw.frame.height/2
            subVw.clipsToBounds = true
        }
        
    }
    
}

extension BaseViewController{
    
    func logOut(){
        self.goToInitialVc()
        //        if (reachability?.isReachable)! {
        //            applicationDelegate.showActivityIndicator()
        //
        //            let parameterDict = ["authorization_key":profileData?.body?.authorization_key!]
        //            AlamoFireWrapperNetwork.sharedInstance.PostDataAlamofire(parameterDict as [String : Any],url: Api.LOGOUT.rawValue, success: { (responseDict,data) in
        //                debugPrint(responseDict ?? "No Value")
        //                let message = responseDict![kMessage].stringValue
        //                if responseDict![status].boolValue == true {
        //                    self.goToInitialVc()
        //                }
        //                else{
        //                    self.showAlert(message: message, type: .error)
        //                }
        //            }, failure: {(error) in
        //                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
        //            })
        //        }
        //        else{
        //            self.showAlert(message: kInternetConnection, type: .error)
        //        }
    }
}
