//
//  Constant.swift
//  Drinks mate
//
//  Created by Rishabh Arora on 1/26/19.
//  Copyright © 2018 Rishabh Arora. All rights reserved.
//

import UIKit

let appName = "Crewtok"
let reachability = Reachability()
let ACCEPTABLE_CHARACTERS = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

//Mark:- Demo Url
//let BASE_URL = "https://dev.crewtok.com/json/"
//let IMG_URL = "https://dev.crewtok.com"

//Mark:- Live Url
let BASE_URL = "https://app.crewtok.com/json/"
let IMG_URL = "https://app.crewtok.com"

let REGISTER_URL = "https://app.crewtok.com/account/register"
let SUPPORT_URL = "https://crewtok.com/contact.html"
let FORGOT_URL = "https://app.crewtok.com/account/forget-password"


let GMSMAP_KEY = "AIzaSyBKwV2w7uWSf3bpgZeRNbMTBKdRbqnmQew"
let GMSPLACES_KEY = "AIzaSyCqcpaTcjy1vKoImiftj6GgQJs8ult59V8"

//MARK:- Constants
let WINDOW_WIDTH = UIScreen.main.bounds.width
let WINDOW_HEIGHT = UIScreen.main.bounds.height

let applicationDelegate = UIApplication.shared.delegate as! AppDelegate
let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)

let UPDATE = "UPDATE"
let CONTINUE = "CONTINUE"
let kMessage = "message"
let KError = "error"

//let kInternetConnection = "Internet connection not available, you must turn on to download app content."
let kInternetConnection = "Internet connection not available, you must enable to download app content."
let kLocationAccess = "Location Access Requested"
let kLocationUnavailable = "Location services are disabled, you must turn on Location Services from Settings."

let WORKINPROGRESS = "Work in progress"
let kOk = "OK"
let CANCEL = "CANCEL"
let SUBMIT = "SUBMIT"
let cameraNotSupported = "Camera Is Not Supported"
let gallery = "Gallery"
let cancel = "Cancel"
let camera = "Camera"
let email = "email"
let status = "status"
let data = "data"
let user = "user"

//MARK:- API
enum Api:String {
    case LOGOUT = "logout"
    case LOGIN = "login"
    case CLOCK_INOUT = "getclockinout"
    case UPDATE_CLOCK_INOUT = "updateclockinout"
    case ASSIGNED_JOBS = "GetassignedJobs"
    
    
    case GET_JOB = "getjob"
    case UPDATE_STATUS = "updatestatus"
    case JOB_CONTACT = "jobcontact"
    case JOB_DESCRIPTION = "jobdescription"
    case VIEW_COMMENT = "viewcomment"
    case ADD_COMMENT = "addcomment"
    case SAVE_DOC = "SavedocByjob"
    case GET_INVENTORY_CATEGORY = "getinventoryCategory"
    case GET_INVENTORY = "getinventorybyCategory"
    case ADD_INVENTORY = "addinventory"
    
//    case VIEW_INVENTORY = "viewinventory"
//    case GET_JOB_STATUS = "Getjobstatus"
    
    case GET_CONTACTS = "Getcontacts"
    case GET_CONTACT_GROUP = "Getcontactgroup"
    case GET_ASSIGNED_EQUIP = "Getassignedequip"
    case GET_DASHBOARD = "getdashboard"
    case ADD_CHAT = "AddChat"
    case GET_CHAT_BY_CONTACT = "getchatbycontact"
    case GET_CHAT_BY_AJAX = "getchatbyajax"
    case UPDATE_LIVE_STATUS = "updatelivestatus"
}

// Mark :- User Defaults

var loginData:Data?{
    get{
        return UserDefaults.standard.value(forKey: "loginData") as? Data
    }set{
        UserDefaults.standard.set(newValue, forKey: "loginData")
    }
}

var loginStatus:Bool?{
    get{
        return UserDefaults.standard.value(forKey: "loginStatus") as? Bool
    }set{
        UserDefaults.standard.set(newValue, forKey: "loginStatus")
    }
}

var sourceLat:Double?{
    get{
        return UserDefaults.standard.value(forKey: "sourceLat") as? Double
    }set{
        UserDefaults.standard.set(newValue, forKey: "sourceLat")
    }
}

var sourceLong:Double?{
    get{
        return UserDefaults.standard.value(forKey: "sourceLong") as? Double
    }set{
        UserDefaults.standard.set(newValue, forKey: "sourceLong")
    }
}

var loginEmail:String?{
    get{
        return UserDefaults.standard.value(forKey: "loginEmail") as? String
    }set{
        UserDefaults.standard.set(newValue, forKey: "loginEmail")
    }
}

var loginPassword:String?{
    get{
        return UserDefaults.standard.value(forKey: "loginPassword") as? String
    }set{
        UserDefaults.standard.set(newValue, forKey: "loginPassword")
    }
}

var sourceLoc:String?{
    get{
        if let loc = UserDefaults.standard.value(forKey: "sourceLoc") as? String{
           return loc
        }
        else{
            return ""
        }
    }set{
        UserDefaults.standard.set(newValue, forKey: "sourceLoc")
    }
}
