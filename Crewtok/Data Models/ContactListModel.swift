//
//  ContactListModel.swift
//  Crewtok
//
//  Created by Rishabh Arora on 28/09/19.
//  Copyright © 2019 Rishabh Arora. All rights reserved.
//

import Foundation

struct ContactListModel:Codable{
    var error:String?
    var data:[ContactListData]?
}

struct ContactListData:Codable {
    var id: Int?
    var name: String?
}
