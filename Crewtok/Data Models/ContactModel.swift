//
//  ContactModel.swift
//  Crewtok
//
//  Created by Rishabh Arora on 28/09/19.
//  Copyright © 2019 Rishabh Arora. All rights reserved.
//

import Foundation

struct ContactModel:Codable{
    var error:String?
    var data:[ContactData]?
}

struct ContactData:Codable {
    var GroupId: Int?
    var id: Int?
    var name: String?
    var pgroup: String?
    var image:String?
}

struct MessageModel:Codable{
    var error:String?
    var data:Messageobj?
}

struct Messageobj:Codable{
    var msgItem:[MessageData]?
}

struct MessageData:Codable {
    var messageId: Int?
    var message: String?
    var sender: Int?
    var Time: String?
    var userId: Int?
    var Name: String?
    var Image:String?
}
