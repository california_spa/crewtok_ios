//
//  EquipmentModel.swift
//  Crewtok
//
//  Created by Rishabh Arora on 28/09/19.
//  Copyright © 2019 Rishabh Arora. All rights reserved.
//

import Foundation
struct EquipmentModel:Codable{
    var error:String?
    var data:[EquipmentData]?
}

struct EquipmentData:Codable {
    var assignedby: String?
    var date: String?
    var name: String?
    var id:Int?
    var quantity:String?
    var availablequntity:String?
    var time:String?
    var serialnumber:String?
    var status:String?
    var token:String?
}
