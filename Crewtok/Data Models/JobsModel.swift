//
//  OffersModel.swift
//  Stadtfalke Vendor
//
//  Created by Rishabh's Mac on 11/05/19.
//  Copyright © 2019 Rishabh Arora. All rights reserved.
//

import Foundation

struct JobsModel:Codable{
    var error:String?
    var data:[JobsData]?
}

struct JobsData:Codable {
    var statuscolor: String?
    var jobId: Int?
    var jobName: String?
    var location:String?
    var image:String?
    var assigndate:String?
    var assignby:String?
    var datecomplete:String?
    var po:String?
    var customername:String?
    var status:String?
    var token:String?
    var phone:String?
}
