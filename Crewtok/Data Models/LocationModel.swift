//
//  LocationModel.swift
//  Stadtfalke Vendor
//
//  Created by Rishabh's Mac on 11/05/19.
//  Copyright © 2019 Rishabh Arora. All rights reserved.
//

import Foundation

struct LocationModel:Codable{
    var message:String?
    var status:Bool?
    var data:[LocationInfo]?
}

struct LocationInfo:Codable {
    var name: String?
    var id: Int?
    var media_id:Int?
    var slug:String?
    var offer_type_ids: String?
    var status:Int?
    var logo_media_image:LogoInfo?
}

struct LogoInfo:Codable {
    var name: String?
    var path:String?
    var type:String?
    var id: Int?
}
