//
//  UserModel.swift
//  Stadtfalke Vendor
//
//  Created by Rishabh's Mac on 05/05/19.
//  Copyright © 2019 Rishabh Arora. All rights reserved.
//

import Foundation

var profileData:ProfileModel?

struct ProfileModel:Codable{
    var data:ProfileData?
    var sessionid:String?
}

struct ProfileData:Codable {
    var Email: String?
    var lastname: String?
    var image: String?
    var firstname:String?
    var name:String?
    var phone:String?
    var address:String?
    var company:String?
}
