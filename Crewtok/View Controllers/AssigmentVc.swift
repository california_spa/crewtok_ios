//
//  AssigmentVc.swift
//  CrewtokApp
//
//  Created by bookyourphone on 17/07/19.
//  Copyright © 2019 manoj. All rights reserved.
//

import UIKit

class assigmentCell: UITableViewCell {
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAssignedBy: UILabel!
}

class AssigmentVc: BaseViewController {
    
    @IBOutlet weak var tblVw: UITableView!
    
    var dataArray = [EquipmentData]()
    var selectedIndexArr = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getData()
        
    }
    
    
}

extension AssigmentVc:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "assigmentCell") as! assigmentCell
        cell.lblName.text = "\(dataArray[indexPath.row].name ?? "") \(dataArray[indexPath.row].serialnumber ?? "")"
        cell.lblDate.text = "\(dataArray[indexPath.row].date ?? "") \(dataArray[indexPath.row].time ?? "")"
        cell.lblAssignedBy.text = "Assigned by: \(dataArray[indexPath.row].assignedby ?? "")"
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 100
        return UITableView.automaticDimension
    }
    
}

extension AssigmentVc{
    
    func getData(){
        
        if (reachability?.isReachable)! {
            applicationDelegate.showActivityIndicator()
            
            let parameterDict = ["userid":profileData?.sessionid ?? ""] as [String : Any]
            AlamoFireWrapperNetwork.sharedInstance.PostDataAlamofire(parameterDict as [String : Any],url: Api.GET_ASSIGNED_EQUIP.rawValue, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    
                    let body = try! JSONDecoder().decode(EquipmentModel.self, from: data!)
                    self.dataArray = body.data!
                    self.tblVw.reloadData()
                    
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
}
