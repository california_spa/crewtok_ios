
//
//  ContactChatVC.swift
//  Crewtok
//
//  Created by Rishabh Arora on 28/08/19.
//  Copyright © 2019 Rishabh Arora. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class SenderTVC: UITableViewCell {
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
}

class ReceiverTVC: UITableViewCell {
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
}

class ContactChatVC: BaseViewController {
    
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var txtVw: UITextView!
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    
    var contactUser = ContactData()
    var dataArray = [MessageData]()
    var timer = Timer()
    var lastMeesageId = 0
    var isFirstTime = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnTitle.setTitle(contactUser.name ?? "", for: .normal)
        
        getData()
        timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true, block: { (time) in
            self.getNewData()
        })
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        
        timer.invalidate()
    }
    
    @objc func  keyboardWillShow(notification : NSNotification)
    {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.bottomConst.constant = keyboardFrame.size.height
        })
        self.tableViewScrollToBottom(animated: false)
    }
    
    @objc func  keyboardWillHide(notification : NSNotification)
    {
        //        self.tableViewScrollToBottom(animated: true)
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.bottomConst.constant = 20
        })
    }
    
    func tableViewScrollToBottom(animated: Bool)
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300))
        {
            let numberOfSections = self.tblVw.numberOfSections
            if numberOfSections > 0
            {
                let numberOfRows = self.tblVw.numberOfRows(inSection: numberOfSections-1)
                
                if numberOfRows > 0
                {
                    let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                    self.tblVw.scrollToRow(at: indexPath, at: .bottom, animated: animated)
                }
            }
        }
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.backBtnAction()
    }
    
    @IBAction func btnSendAction(_ sender: Any) {
        if txtVw.text.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            self.showAlert(message: "Please enter message", type: .error)
        }
        else{
            apiSendMessageData()
        }
    }
    
    
}

extension ContactChatVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var maincell = UITableViewCell()
        let obj = dataArray[indexPath.row]
        
        if "\(obj.sender!)" == profileData?.sessionid ?? ""{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SenderTVC") as! SenderTVC
            cell.lblName.text = (dataArray[indexPath.row].message ?? "").parseHtmlString
            cell.lblTime.text = dataArray[indexPath.row].Time ?? ""
            cell.imgUser.sd_setImage(with: URL(string: dataArray[indexPath.row].Image ?? "")!, placeholderImage: #imageLiteral(resourceName: "dummy_profile"))
            maincell = cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverTVC") as! ReceiverTVC
            cell.lblName.text = (dataArray[indexPath.row].message ?? "").parseHtmlString
            cell.lblTime.text = dataArray[indexPath.row].Time ?? ""
            cell.imgUser.sd_setImage(with: URL(string: dataArray[indexPath.row].Image ?? "")!, placeholderImage: #imageLiteral(resourceName: "dummy_profile"))
            maincell = cell
        }
        
        if indexPath.row == dataArray.count - 1{
            lastMeesageId = obj.messageId!
        }
        
        maincell.selectionStyle = .none
        return maincell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tblVw.estimatedRowHeight = 50
        return UITableView.automaticDimension
    }
    
}

extension ContactChatVC{
    
    func getData(){
        
        if (reachability?.isReachable)! {
            if isFirstTime{
                isFirstTime = false
                applicationDelegate.showActivityIndicator()
            }
            
            let parameterDict = ["userid":profileData?.sessionid ?? "","contactId":contactUser.id ?? "","page":"1"] as [String : Any]
            
            AlamoFireWrapperNetwork.sharedInstance.PostDataAlamofire(parameterDict as [String : Any],url: Api.GET_CHAT_BY_CONTACT.rawValue, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    
                    let body = try! JSONDecoder().decode(MessageModel.self, from: data!)
                    self.dataArray = body.data?.msgItem ?? self.dataArray
                    self.reloadData()
                    
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
    func getNewData(){
        
        if (reachability?.isReachable)! {
            //            applicationDelegate.showActivityIndicator()
            
            let parameterDict = ["userid":profileData?.sessionid ?? "","contactId":contactUser.id ?? "","maxid":lastMeesageId] as [String : Any]
            
            AlamoFireWrapperNetwork.sharedInstance.PostDataAlamofire(parameterDict as [String : Any],url: Api.GET_CHAT_BY_AJAX.rawValue, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    
                    let body = try! JSONDecoder().decode(MessageModel.self, from: data!)
                    if (body.data?.msgItem!.count)! > 0{
                        self.dataArray.append(body.data?.msgItem?[0] ?? MessageData())
                        self.reloadData()
                    }
                    
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
    func apiSendMessageData(){
        
        if (reachability?.isReachable)! {
            applicationDelegate.showActivityIndicator()
            
            let parameterDict = ["userid":profileData?.sessionid ?? "","contactId":contactUser.id ?? "","Text":txtVw.text!] as [String : Any]
            
            AlamoFireWrapperNetwork.sharedInstance.PostDataAlamofire(parameterDict as [String : Any],url: Api.ADD_CHAT.rawValue, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    self.txtVw.text = ""
                    let body = try! JSONDecoder().decode(MessageModel.self, from: data!)
                    if (body.data?.msgItem!.count)! > 0{
                        self.dataArray.append(body.data?.msgItem?[0] ?? MessageData())
                        self.reloadData()
                    }
                    
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
    func reloadData(){
        self.tblVw.reloadData()
        if dataArray.count > 0{
            self.tableViewScrollToBottom(animated: false)
        }
    }
    
}
