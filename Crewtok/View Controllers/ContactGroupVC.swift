
//
//  ContactGroupVC.swift
//  Crewtok
//
//  Created by Rishabh Arora on 28/08/19.
//  Copyright © 2019 Rishabh Arora. All rights reserved.
//

import UIKit

class contactGroupCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
}

class ContactGroupVC: BaseViewController {
    
    @IBOutlet weak var tblVw: UITableView!
    
    var dataArray = [ContactListData]()
    var selectedIndexArr = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getData()
        
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.backBtnAction()
    }
    
    @IBAction func btnApplyAction(_ sender: Any) {
        self.backBtnAction()
        if selectedIndexArr.count > 0{
            let group = self.dataArray.filter{$0.id! == selectedIndexArr[0] as! Int}
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateContactList"), object: group[0])
        }
        else{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateContactList"), object: nil)
        }
    }
    
    
}

extension ContactGroupVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! contactGroupCell
        
        if indexPath.row != 0{
            cell.lblName.text = dataArray[indexPath.row - 1].name ?? ""
            if selectedIndexArr.contains(dataArray[indexPath.row - 1].id!){
                cell.btnCheck.isSelected = true
            }
            else{
                cell.btnCheck.isSelected = false
            }
        }
        else{
            cell.lblName.text = "All"
            if selectedIndexArr.count == 0{
                cell.btnCheck.isSelected = true
            }
            else{
                cell.btnCheck.isSelected = false
            }
        }
        
        cell.btnCheck.addTarget(self, action: #selector(selectGroup(_:)), for: .touchUpInside)
        cell.btnCheck.tag = indexPath.row
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexArr = []
        if indexPath.row != 0{
            selectedIndexArr.add(dataArray[indexPath.row - 1].id!)
        }
        tblVw.reloadData()
    }
    
    @objc func selectGroup(_ sender:UIButton){
        selectedIndexArr = []
        if sender.tag != 0{
            selectedIndexArr.add(dataArray[sender.tag - 1].id!)
        }
        tblVw.reloadData()
    }
    
}

extension ContactGroupVC{
    
    func getData(){
        
        if (reachability?.isReachable)! {
            applicationDelegate.showActivityIndicator()
            
            let parameterDict = ["userid":profileData?.sessionid ?? ""] as [String : Any]
            AlamoFireWrapperNetwork.sharedInstance.PostDataAlamofire(parameterDict as [String : Any],url: Api.GET_CONTACT_GROUP.rawValue, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    
                    let body = try! JSONDecoder().decode(ContactListModel.self, from: data!)
                    self.dataArray = body.data!
                    self.tblVw.reloadData()
                    
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
}
