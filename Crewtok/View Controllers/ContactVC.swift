//
//  ContactVC.swift
//  CrewtokApp
//
//  Created by cql on 18/07/19.
//  Copyright © 2019 manoj. All rights reserved.
//

import UIKit

class contactCell: UITableViewCell {
    @IBOutlet weak var imgContact: UIImageViewCustomClass!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCat: UILabel!
}

class ContactVC:
BaseViewController {
    
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var lblGroupName: UIButton!
    
    var dataArray = [ContactData]()
    var selectedGroupId = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateList(_:)), name: NSNotification.Name(rawValue: "updateContactList"), object: nil)
        
        getData()
        
    }
    
    @IBAction func btnSelectCategoryAction(_ sender: Any) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ContactGroupVC") as! ContactGroupVC
        vc.selectedIndexArr = selectedGroupId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func updateList(_ noti:NSNotification){
        selectedGroupId = []
        
        if let obj = noti.object as? ContactListData{
        self.lblGroupName.setTitle("Contact (\(obj.name ?? ""))", for: .normal)
            selectedGroupId.add(obj.id!)
          getData(obj.id!)
        }
        else{
            self.lblGroupName.setTitle("Contact (All)", for: .normal)
           getData()
        }
    }
    
}

extension ContactVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell") as! contactCell
        cell.lblName.text = dataArray[indexPath.row].name ?? ""
        cell.lblCat.text = dataArray[indexPath.row].pgroup ?? ""
        cell.imgContact.sd_setImage(with: URL(string: dataArray[indexPath.row].image ?? "")!, placeholderImage: #imageLiteral(resourceName: "dummy_profile"))
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ContactChatVC") as! ContactChatVC
        vc.contactUser = dataArray[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension ContactVC{
    
    func getData(_ groupid:Int? = nil){
        
        if (reachability?.isReachable)! {
            applicationDelegate.showActivityIndicator()
            
            var parameterDict = ["userid":profileData?.sessionid ?? ""] as [String : Any]
            if let gId = groupid{
                parameterDict["groupid"] = gId
            }
            AlamoFireWrapperNetwork.sharedInstance.PostDataAlamofire(parameterDict as [String : Any],url: Api.GET_CONTACTS.rawValue, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    
                    let body = try! JSONDecoder().decode(ContactModel.self, from: data!)
                    self.dataArray = body.data!
                    self.tblVw.reloadData()
                    if self.dataArray.count == 0{
                    self.showAlert(message: "No Contacts available", type: .info)
                    }
                    
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
}



