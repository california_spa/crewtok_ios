

//
//  ContainerViewVC.swift
//  Crewtok
//
//  Created by Rishabh Arora on 25/09/19.
//  Copyright © 2019 Rishabh Arora. All rights reserved.
//

import UIKit
import SDWebImage

class ContainerViewVC: BaseViewController {
    
    @IBOutlet weak var btnProfile: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let imgStr = "\(IMG_URL)\((profileData?.data?.image ?? "").replacingOccurrences(of: "~", with: ""))"
            btnProfile.sd_setImage(with: URL(string: imgStr), for: .normal, placeholderImage: #imageLiteral(resourceName: "dummy_profile"))
    }
    

    func updateImage(_ img:String){
        let imgStr = "\(IMG_URL)\((profileData?.data?.image ?? "").replacingOccurrences(of: "~", with: ""))"
        btnProfile.sd_setImage(with: URL(string: imgStr), for: .normal, placeholderImage: #imageLiteral(resourceName: "dummy_profile"))
    }

}
