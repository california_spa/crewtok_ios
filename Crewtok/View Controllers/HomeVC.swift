//
//  HomeVC.swift
//  CrewtokApp
//
//  Created by bookyourphone on 17/07/19.
//  Copyright © 2019 manoj. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class HomeCollVwCell: UICollectionViewCell {
    @IBOutlet weak var Titlelbl: UILabel!
    @IBOutlet weak var aiggnlbl: UILabel!
    @IBOutlet weak var imgCat: UIImageView!
}

class HomeVC: BaseViewController{
    
    let array = ["Time clock","Jobs","Communicate","Equipment"]
    var newArray = ["","Assigned:1","Unread Chat:1","Equipment:1"]
    let imageArray = [UIImage(named: "clock"),UIImage(named: "jobs")!,UIImage(named: "chat")!,UIImage(named: "equipment")!]
    
    var containerViewController: ContainerViewVC?
    var timer = Timer()
    var isFirstTime = true
    var otherId = Int()
    
    @IBOutlet weak var CollV: UICollectionView!
    @IBOutlet weak var lblLastMessage: UILabel!
    @IBOutlet weak var txtFldMessage: UITextFieldCustomClass!
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    @IBOutlet weak var topConst: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.getData()
        timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true, block: { (time) in
            self.getData()
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        
        timer.invalidate()
    }
    
    @objc func  keyboardWillShow(notification : NSNotification)
    {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.bottomConst.constant = keyboardFrame.size.height
            self.topConst.constant = -keyboardFrame.size.height
        })
//        self.tableViewScrollToBottom(animated: false)
    }
    
    
    @objc func  keyboardWillHide(notification : NSNotification)
    {
//        self.tableViewScrollToBottom(animated: true)
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.bottomConst.constant = 0
            self.topConst.constant = 64
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        DispatchQueue.main.async {
            if sourceLoc! != ""{
                if applicationDelegate.isTimeClock{
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "TimeClockVC") as! TimeClockVC
                    vc.modalTransitionStyle = .crossDissolve
                    vc.modalPresentationStyle = .overCurrentContext
                    self.present(vc, animated: true, completion: nil)
                    applicationDelegate.isTimeClock = false
                }
            }
            else{
                self.showAlert(message: kLocationUnavailable, type: .info)
            }
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! ContainerViewVC
        containerViewController = vc
    }
    
    @IBAction func btnReplyAction(_ sender: Any) {
        if txtFldMessage.isEmpty{
            self.showAlert(message: "Please enter reply", type: .error)
        }
        else{
            apiSendMessageData()
        }
    }
    
    
}

extension HomeVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollVwCell", for: indexPath) as! HomeCollVwCell
        cell.Titlelbl.text = array[indexPath.row]
        cell.aiggnlbl.text = newArray[indexPath.row]
        cell.imgCat.image = imageArray[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.CollV.frame.width/2 - 5, height: self.CollV.frame.width/2 - 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row  == 0 {
            if sourceLoc! != ""{
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "TimeClockVC") as! TimeClockVC
                vc.modalTransitionStyle = .crossDissolve
                vc.modalPresentationStyle = .overCurrentContext
                self.present(vc, animated: true, completion: nil)
            }
            else{
                self.showAlert(message: kLocationUnavailable, type: .info)
            }
        }
        else if indexPath.row  == 1 {
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "AssignedVc") as! AssignedVc
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row  == 2 {
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ContactVC") as! ContactVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row  == 3 {
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "AssigmentVc") as! AssigmentVc
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    
}

extension HomeVC{
    
    func getData(){
        
        if (reachability?.isReachable)! {
            if isFirstTime{
                isFirstTime = false
                applicationDelegate.showActivityIndicator()
            }
            
            let parameterDict = ["userid":profileData?.sessionid ?? ""] as [String : Any]
            AlamoFireWrapperNetwork.sharedInstance.PostDataAlamofire(parameterDict as [String : Any],url: Api.GET_DASHBOARD.rawValue, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                   
//                    self.containerViewController?.updateImage(responseDict!["data"]["profileimage"].stringValue)
                    self.lblLastMessage.text = responseDict!["data"]["latest_message"].stringValue.parseHtmlString
                    self.otherId = responseDict!["data"]["chatcontactid"].intValue
                    
                    applicationDelegate.lastClockedMessage = "\(responseDict!["data"]["lastClockedMessage"].stringValue)"
                    applicationDelegate.assignjobs = "\(responseDict!["data"]["assignjobs"].stringValue)"
                    
                    self.newArray = ["","Assigned:\(responseDict!["data"]["assignjobs"].stringValue)","Unread Chat:\(responseDict!["data"]["unreadchat"].stringValue)","Equipment:\(responseDict!["data"]["equipments"].stringValue)"]
                    
                    self.CollV.delegate = self
                    self.CollV.dataSource = self
                    self.CollV.reloadData()
                }
                else{
//                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
//                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
    func apiSendMessageData(){
        
        if (reachability?.isReachable)! {
            applicationDelegate.showActivityIndicator()
            
            let parameterDict = ["userid":profileData?.sessionid ?? "","contactId":otherId,"Text":txtFldMessage.text!] as [String : Any]
            
            AlamoFireWrapperNetwork.sharedInstance.PostDataAlamofire(parameterDict as [String : Any],url: Api.ADD_CHAT.rawValue, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    self.txtFldMessage.text = ""
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
}
