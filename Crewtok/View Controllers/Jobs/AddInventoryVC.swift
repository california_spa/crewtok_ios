
//
//  AddInventoryVC.swift
//  Crewtok
//
//  Created by Rishabh Arora on 28/08/19.
//  Copyright © 2019 Rishabh Arora. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddInventoryVC: BaseViewController {
    
    var jobData = JobsData()
    var totalQuantity = 0
    var categoryArray = [JSON]()
    var inventoryArray = [JSON]()
    var selectedCategory = ""
    var selectedInventory = ""
    
    var selectedFld = 0
    var pickerVw = UIPickerView()
    var detailVC = JobDetailVC()
    
    @IBOutlet weak var txtFldCategory: UITextFieldCustomClass!
    @IBOutlet weak var txtFldInventory: UITextFieldCustomClass!
    @IBOutlet weak var txtFldQuantity: UITextFieldCustomClass!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getCategoryData()
    }
    
    
    @IBAction func btnCrossAction(_ sender: UIButton) {
        self.dismissBtnAction()
    }
    
    @IBAction func btnSubmitAction(_ sender: UIButton){
        if txtFldCategory.isEmpty{
            self.showAlert(message: "Please select category", type: .error)
        }
        else if txtFldInventory.isEmpty{
            self.showAlert(message: "Please select inventory", type: .error)
        }
        else if txtFldQuantity.isEmpty{
            self.showAlert(message: "Please enter quantity", type: .error)
        }
        else if let value = Int(txtFldQuantity.text!){
            if value > totalQuantity{
                self.showAlert(message: "Total available quantity is \(totalQuantity)", type: .info)
            }
            else{
                self.addInventory()
            }
            
        }
    }
    

}

extension AddInventoryVC:UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtFldCategory{
            selectedFld = 0
        }
        else if textField == txtFldInventory{
            selectedFld = 1
        }
        textField.inputView = pickerVw
        pickerVw.delegate = self
        pickerVw.dataSource = self
        pickerVw.reloadAllComponents()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if selectedFld == 0{
          return categoryArray.count
        }
        else{
           return inventoryArray.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if selectedFld == 0{
            return categoryArray[row]["Tagname"].stringValue
        }
        else{
            return inventoryArray[row]["name"].stringValue
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if selectedFld == 0{
           txtFldCategory.text = categoryArray[row]["Tagname"].stringValue
            selectedCategory = categoryArray[row]["tagid"].stringValue
            self.getInventoryData(categoryArray[row]["tagid"].stringValue)
        }
        else{
            txtFldInventory.text = inventoryArray[row]["name"].stringValue
            selectedInventory = inventoryArray[row]["id"].stringValue
            totalQuantity = inventoryArray[row]["Quantity"].intValue
        }
    }
    
}


extension AddInventoryVC{
    
    func getCategoryData(){
        
        if (reachability?.isReachable)! {
            applicationDelegate.showActivityIndicator()
            
            let url = "\(Api.GET_INVENTORY_CATEGORY.rawValue)?userid=\(profileData?.sessionid ?? "")"
            
            AlamoFireWrapperNetwork.sharedInstance.GetDataAlamofire(url: url, isHide:false, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    
                    self.categoryArray = responseDict!["data"].arrayValue
                    if let obj = self.categoryArray.first{
                        self.txtFldCategory.text = obj["Tagname"].stringValue
                        self.selectedCategory = obj["tagid"].stringValue
                        self.getInventoryData(obj["tagid"].stringValue)
                    }
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
    func getInventoryData(_ catId:String){
        
        if (reachability?.isReachable)! {
//            applicationDelegate.showActivityIndicator()
            
            let url = "\(Api.GET_INVENTORY.rawValue)?userid=\(profileData?.sessionid ?? "")&groupId=\(catId)"
            
            AlamoFireWrapperNetwork.sharedInstance.GetDataAlamofire(url: url, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    
                    self.inventoryArray = responseDict!["data"].arrayValue
                    if let obj = self.inventoryArray.first{
                        self.txtFldInventory.text = obj["name"].stringValue
                        self.selectedInventory = obj["id"].stringValue
                        self.totalQuantity = obj["Quantity"].intValue
                    }
                    
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
    func addInventory(){
        
        if (reachability?.isReachable)! {
            applicationDelegate.showActivityIndicator()
            
            let parameterDict = ["id":selectedInventory,"jobid":jobData.jobId!,"Quantity":txtFldQuantity.text!] as [String : Any]
            AlamoFireWrapperNetwork.sharedInstance.PostDataAlamofire(parameterDict as [String : Any],url: Api.ADD_INVENTORY.rawValue, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                   
                    self.detailVC.getData()
                    self.dismissBtnAction()
                    
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
}
