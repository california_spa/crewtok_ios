//
//  AssignedVc.swift
//  CrewtokApp
//
//  Created by bookyourphone on 16/07/19.
//  Copyright © 2019 manoj. All rights reserved.
//

import UIKit

class assignedCell: UITableViewCell {
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblJobNo: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
}

class AssignedVc: BaseViewController{
    
    @IBOutlet weak var tblVw: UITableView!
    
    var dataArray = [JobsData]()
    var selectedIndexArr = NSMutableArray()
   
    override func viewDidLoad() {
        super.viewDidLoad()

        getData()

    }

    
    @IBAction func btnNextAction(_ sender: UIButton) {
        if selectedIndexArr.count > 0{
        let selectedJobs = dataArray.filter({selectedIndexArr.contains($0.jobId!)})
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ReorderVc") as! ReorderVc
            vc.dataArray = selectedJobs
        self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            self.showAlert(message: "At-least one location must be selected", type: .info)
        }
    }
    
}

extension AssignedVc:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "assignedCell") as! assignedCell
        cell.lblName.text = dataArray[indexPath.row].jobName ?? ""
        cell.lblDateTime.text = dataArray[indexPath.row].assigndate ?? ""
        cell.lblJobNo.text = "# \(dataArray[indexPath.row].po ?? "")"
        cell.lblAddress.text = dataArray[indexPath.row].location ?? ""
        if selectedIndexArr.contains(dataArray[indexPath.row].jobId!){
            cell.btnCheck.isSelected = true
        }
        else{
            cell.btnCheck.isSelected = false
        }
        cell.btnCheck.addTarget(self, action: #selector(selectJob(_:)), for: .touchUpInside)
        cell.btnCheck.tag = indexPath.row
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 80
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "JobDetailVC") as! JobDetailVC
        vc.jobData = dataArray[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func selectJob(_ sender:UIButton){
        let jobId = dataArray[sender.tag].jobId!
        if selectedIndexArr.contains(jobId){
            selectedIndexArr.remove(jobId)
        }
        else{
            selectedIndexArr.add(jobId)
        }
        tblVw.reloadData()
    }
    
}

extension AssignedVc{
    
    func getData(){
        
        if (reachability?.isReachable)! {
            applicationDelegate.showActivityIndicator()
            
            let parameterDict = ["userid":profileData?.sessionid ?? ""] as [String : Any]
            AlamoFireWrapperNetwork.sharedInstance.PostDataAlamofire(parameterDict as [String : Any],url: Api.ASSIGNED_JOBS.rawValue, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    
                    let body = try! JSONDecoder().decode(JobsModel.self, from: data!)
                    self.dataArray = body.data!
                    self.tblVw.reloadData()
                    
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
}
    
}
