//
//  JobDetailVC.swift
//  Crewtok
//
//  Created by Rishabh Arora on 28/08/19.
//  Copyright © 2019 Rishabh Arora. All rights reserved.
//

import UIKit
import SwiftyJSON
import GSImageViewerController
import MobileCoreServices


class JobDetailCVC:UICollectionViewCell{
    @IBOutlet weak var imgAttachment: UIImageView!
}

class JobDetailVC: BaseViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    let newColor = UIColor(displayP3Red: 49.0/255.0, green: 142.0/255.0, blue: 239.0/255.0, alpha: 1.0)
    let progressColor = UIColor(displayP3Red: 255.0/255.0, green: 206.0/255.0, blue: 10.0/255.0, alpha: 1.0)
    let problemColor = UIColor.red
    let completeColor = UIColor(displayP3Red: 0.0/255.0, green: 229.0/255.0, blue: 70.0/255.0, alpha: 1.0)
    
    @IBOutlet weak var btnComplete: UIButtonCustomClass!
    @IBOutlet weak var btnProblem: UIButtonCustomClass!
    @IBOutlet weak var btrnProgres: UIButtonCustomClass!
    @IBOutlet weak var btnNew: UIButtonCustomClass!
    @IBOutlet weak var stckStatus: UIStackView!
    @IBOutlet weak var cmntImgWidth: NSLayoutConstraint!
    @IBOutlet weak var clctnvwAttachhment: UICollectionView!
    
    @IBOutlet weak var btnJobTitle: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblInventoryType: UILabel!
    @IBOutlet weak var lblEquipmentReq: UILabel!
    @IBOutlet weak var txtFldComment: UITextFieldCustomClass!
    @IBOutlet weak var lblLatestComment: UILabel!
    
    
    var jobData = JobsData()
    var attachmentArray = [JSON]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnJobTitle.setTitle(jobData.jobName ?? "", for: .normal)
        cmntImgWidth.constant = 0
        
        getData()
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.backBtnAction()
    }
    
    @IBAction func btnVwComments(_ sender: Any) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ViewCommentsVC") as! ViewCommentsVC
        vc.jobData = jobData
        vc.detailVC = self
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func btnViewContact(_ sender: Any) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ViewContactVC") as! ViewContactVC
        vc.jobData = jobData
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnViewDescAction(_ sender: Any) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ViewDescriptionVC") as! ViewDescriptionVC
        vc.jobData = jobData
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnInventoryAction(_ sender: Any) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddInventoryVC") as! AddInventoryVC
        vc.jobData = jobData
        vc.detailVC = self
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnCommentCameraAction(_ sender: Any) {
        if txtFldComment.isEmpty{
            self.showAlert(message: "Please enter comment", type: .error)
        }
        else{
            apiCommentData()
        }
    }
    
    @IBAction func btnAttachmentCameraAction(_ sender: Any) {
       MyImagePicker()
        
    }
    
    // MARK:- ImagePicker
    func MyImagePicker() {
        let alert:UIAlertController=UIAlertController(title: "Choose a picture", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGallery()
        }
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera()
        }
        let docAction = UIAlertAction(title: "Document", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openDocPicker()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }
        
        alert.addAction(galleryAction)
        alert.addAction(cameraAction)
        alert.addAction(docAction)
        alert.addAction(cancelAction)
        alert.view.tintColor = .black
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            let imagePicker = UIImagePickerController()
            imagePicker.navigationBar.tintColor = UIColor.black
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            self .present(imagePicker, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Warning", message: "You do not have a camera" , preferredStyle: .actionSheet)
            let secondAction = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction?) -> Void in
                
            })
            alert.addAction(secondAction)
            self.present(alert, animated: true)
        }
    }
    
    func openGallery() {
        let imagePicker = UIImagePickerController()
        imagePicker.navigationBar.tintColor = UIColor.black
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func openDocPicker(){
        let types = [kUTTypePDF, kUTTypeRTFD,kUTTypeFlatRTFD,kUTTypeTXNTextAndMultimediaData,kUTTypeImage]
        //        let types = ["public.image","public.item","public.data", "public.content","public.text"]
        let docMenu = UIDocumentPickerViewController(documentTypes: types as [String], in: .import)
        docMenu.allowsMultipleSelection = false
        docMenu.delegate = self
        docMenu.modalPresentationStyle = .formSheet
        present(docMenu, animated: true)
    }
    
    @IBAction func btnStatusAction(_ sender: UIButton) {
        self.refreshStatus()
        self.updateStatusApi(sender.tag + 1)
    }
    
    func refreshStatus(){
        btnComplete.setTitleColor(completeColor, for: .normal)
        btnProblem.setTitleColor(problemColor, for: .normal)
        btrnProgres.setTitleColor(progressColor, for: .normal)
        btnNew.setTitleColor(newColor, for: .normal)
        
        for vw in stckStatus.arrangedSubviews{
            vw.backgroundColor = UIColor.white
        }
    }
    

}


extension JobDetailVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return attachmentArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! JobDetailCVC
        let data = attachmentArray[indexPath.row]
        cell.imgAttachment.sd_setImage(with: URL(string: data["url"].stringValue)!, placeholderImage: #imageLiteral(resourceName: "file"))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: 55)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = attachmentArray[indexPath.row]
        let url = data["url"].stringValue
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebVwVC") as! WebVwVC
        vc.urlStr = url
        self.navigationController?.pushViewController(vc, animated: true)
        
//        let cell = collectionView.cellForItem(at: indexPath) as! JobDetailCVC
//        let imageInfo = GSImageInfo(image: cell.imgAttachment.image!, imageMode: .aspectFit, imageHD: nil)
//        let imageViewer    = GSImageViewerController(imageInfo: imageInfo)
//        present(imageViewer, animated: true, completion: nil)
    }
    
    
}


extension JobDetailVC:UIDocumentPickerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var imgPick = UIImage()
        if info[.editedImage] != nil{
            imgPick = info[.editedImage] as! UIImage
        }else{
            imgPick = info[.originalImage] as! UIImage
        }
        imgPick = resizeImageToCenter(imgPick)
        dismissBtnAction()
        
        if let data = imgPick.pngData(){
        self.apiSaveDoc(data, ext: "jpeg")
        }
    }
    
    func resizeImageToCenter(_ image: UIImage) -> UIImage {
        let size = CGSize(width: 200 , height: 200)
        
        // Define rect for thumbnail
        let scale = max(size.width/image.size.width, size.height/image.size.height)
        let width = image.size.width * scale
        let height = image.size.height * scale
        let x = (size.width - width) / CGFloat(2)
        let y = (size.height - height) / CGFloat(2)
        let thumbnailRect = CGRect.init(x: x, y: y, width: width, height: height)
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        image.draw(in: thumbnailRect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print(urls[0])
        let fullPath = "\(urls[0])"
        let fileExt = fullPath.fileExtension()
        self.sendDocuments(URL.init(string: fullPath)!, ext: fileExt)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        
    }
    
    func sendDocuments(_ documentsPath : URL,ext:String) -> Void {
        let documentsData: NSData?
        do {
            var _: NSError?
            
            documentsData = try NSData(contentsOfFile: ("\(documentsPath.path)"), options: NSData.ReadingOptions.alwaysMapped)
            
            let data = Data(referencing: documentsData!)
            self.apiSaveDoc(data, ext: ext)
            
            
        } catch {
            print(error)
            documentsData = nil
            return
        }
        
        print("Document Selected")
    }
    
}

extension String {
    
    func fileName() -> String {
        return NSURL(fileURLWithPath: self).deletingPathExtension?.lastPathComponent ?? ""
    }
    
    func fileExtension() -> String {
        return NSURL(fileURLWithPath: self).pathExtension ?? ""
    }
    
}

extension JobDetailVC{
    
    func getData(){
        
        if (reachability?.isReachable)! {
            applicationDelegate.showActivityIndicator()
            
            let url = "\(Api.GET_JOB.rawValue)?jobid=\(jobData.jobId ?? 0)"
            
            AlamoFireWrapperNetwork.sharedInstance.GetDataAlamofire(url: url, isHide:false, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    
                    self.lblDate.text = responseDict!["data"]["date"].stringValue
                    self.lblTime.text = responseDict!["data"]["time"].stringValue
                    self.lblLocation.text = responseDict!["data"]["Location"].stringValue.parseHtmlString
                    self.lblDesc.text = responseDict!["data"]["description"].stringValue.parseHtmlString
                    self.lblInventoryType.text = responseDict!["data"]["date"].stringValue
                    self.lblLatestComment.text = responseDict!["data"]["latestComment"].stringValue.parseHtmlString
                    
                    var equipTypes = ""
                    let equipArr = responseDict!["data"]["equipments"].arrayValue
                    for obj in equipArr{
                        equipTypes = "\(equipTypes)\(equipTypes == "" ? "" : ",")\(obj["name"].stringValue)"
                    }
                    self.lblEquipmentReq.text = equipTypes
                    
                    var inventoryTypes = ""
                    let inventoryArr = responseDict!["data"]["inventory"].arrayValue
                    for obj in inventoryArr{
                        inventoryTypes = "\(inventoryTypes)\(inventoryTypes == "" ? "" : ",")\(obj["name"].stringValue)"
                    }
                    self.lblInventoryType.text = inventoryTypes
                    
                    let status = responseDict!["data"]["statusid"].stringValue
                    switch status{
                    case "1":
                        self.btnNew.backgroundColor = self.newColor
                        self.btnNew.setTitleColor(UIColor.white, for: .normal)
                    case "2":
                        self.btrnProgres.backgroundColor = self.progressColor
                        self.btrnProgres.setTitleColor(UIColor.white, for: .normal)
                    case "3":
                        self.btnProblem.backgroundColor = self.problemColor
                        self.btnProblem.setTitleColor(UIColor.white, for: .normal)
                    case "4":
                        self.btnComplete.backgroundColor = self.completeColor
                        self.btnComplete.setTitleColor(UIColor.white, for: .normal)
                    default:
                        break
                    }
                    
                    self.attachmentArray = responseDict!["data"]["attachments"].arrayValue
                    self.clctnvwAttachhment.reloadData()
                    applicationDelegate.hideActivityIndicator()
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
    func updateStatusApi(_ status:Int){
        
        if (reachability?.isReachable)! {
            applicationDelegate.showActivityIndicator()
            
            let parameterDict = ["jobid":jobData.jobId!,"statusid":status] as [String : Any]
            AlamoFireWrapperNetwork.sharedInstance.PostDataAlamofire(parameterDict as [String : Any],url: Api.UPDATE_STATUS.rawValue, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    
                    self.getData()
                    
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
    func apiCommentData(){
        
        if (reachability?.isReachable)! {
            applicationDelegate.showActivityIndicator()
            
            let parameterDict = ["userid":profileData?.sessionid ?? "","jobid":jobData.jobId!,"body":txtFldComment.text!,"file":""] as [String : Any]
            
            AlamoFireWrapperNetwork.sharedInstance.PostDataAlamofire(parameterDict as [String : Any],url: Api.ADD_COMMENT.rawValue, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    self.txtFldComment.text = ""
                    self.getData()
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
    func apiSaveDoc(_ docData:Data,ext:String){
        if (reachability?.isReachable)! {
            applicationDelegate.showActivityIndicator()
            
            let parameterDict = ["Jobid":"\(jobData.jobId!)"]
            AlamoFireWrapperNetwork.sharedInstance.PostMultipartData(parameterDict, url: Api.SAVE_DOC.rawValue, headers: nil, imgFileParam: "file", document:docData, docExt: ext, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    
                    self.getData()
                    
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
}
