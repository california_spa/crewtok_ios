//
//  ViewController.swift
//  CrewtokApp
//
//  Created by bookyourphone on 14/07/19.
//  Copyright © 2019 manoj. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import GooglePlaces

extension CharacterSet {
    static let googleAllowed = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~")
}

class ReOrderMapVC: BaseViewController {
    
    var anotherLocationStr = ""
    var anotherLocation = CLLocationCoordinate2D()
    var locType = 1
    
    var dataArray = [JobsData]()
    
    @IBOutlet weak var lblCurrentLocation: UILabel!
    @IBOutlet weak var lblAnotherLoc: UILabel!
    @IBOutlet weak var btnCurrent: UIButtonCustomClass!
    @IBOutlet weak var btnOther: UIButtonCustomClass!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        lblCurrentLocation.text = "1. Select current location \n\(sourceLoc!)"
    }
    
    @IBAction func btnCrossAction(_ sender: UIButton) {
        self.removeFromParentBtnAction()
    }
    
    @IBAction func btnSelectLocation(_ sender: UIButtonCustomClass) {
        if sender.tag == 0{
            locType = 1
            btnCurrent.setTitle("X", for: .normal)
            btnOther.setTitle("", for: .normal)
        }
        else{
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            present(autocompleteController, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        
        var source = ""
        var dest = ""
        var waypoints = ""
        
        if locType == 1{
            source = "origin=\(sourceLoc!)"
        }
        else{
            source = "origin=\(anotherLocationStr)"
        }
        
        if dataArray.count > 1{
            for (index,obj) in dataArray.enumerated(){
                if index != dataArray.count - 1{
                    waypoints = "\(waypoints)\(waypoints == "" ? "" : "%7C")\(obj.location ?? "")"
                }
            }
            dest = "destination=\(dataArray.last?.location ?? "")"
        }
        else{
            dest = "destination=\(dataArray.first?.location ?? "")"
        }
        
        waypoints = "waypoints=\(waypoints)"
        
//        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {            UIApplication.shared.open(URL(string:"comgooglemaps://?api=1&\(source)&\(dest)&\(waypoints)&travelmode=driving&dir_action=navigate".replacingOccurrences(of: " ", with: "+"))!)
//        } else {
            UIApplication.shared.open(URL(string:"https://www.google.com/maps/dir/?api=1&\(source)&\(dest)&\(waypoints)&travelmode=driving&dir_action=navigate".replacingOccurrences(of: " ", with: "+"))!)
            NSLog("Can't use comgooglemaps://");
//        }
        
    }
    
    func appleMap(){
        //        let latitude:CLLocationDegrees =  37.3230
        //        let longitude:CLLocationDegrees =  122.0322
        //
        //        let regionDistance:CLLocationDistance = 10000
        //        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        //        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        //        let options = [
        //            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
        //            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        //        ]
        //        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        //        let mapItem = MKMapItem(placemark: placemark)
        //        mapItem.name = ""
        //        mapItem.openInMaps(launchOptions: options)
    }
    
    
}

//MARK:- Address Picker Extension

extension ReOrderMapVC: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        locType = 2
        btnCurrent.setTitle("", for: .normal)
        btnOther.setTitle("X", for: .normal)
        self.anotherLocationStr = place.formattedAddress ?? ""
        self.anotherLocation = place.coordinate
        self.lblAnotherLoc.text = "2. Set another location \n\(place.formattedAddress ?? "")"
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
