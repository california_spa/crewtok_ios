//
//  ReorderVc.swift
//  CrewtokApp
//
//  Created by bookyourphone on 16/07/19.
//  Copyright © 2019 manoj. All rights reserved.
//

import UIKit
import SwiftReorder

class ReorderVc: BaseViewController {
    
    @IBOutlet weak var tblVw: UITableView!
    
    var dataArray = [JobsData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblVw.isEditing = true
        tblVw.reorder.delegate = self
        tblVw.reorder.cellScale = 1.05
        tblVw.reorder.shadowOpacity = 20
        tblVw.reorder.shadowRadius = 20

    }
    
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ReOrderMapVC") as! ReOrderMapVC
        vc.dataArray = dataArray
        self.addChild(vc)
        vc.didMove(toParent: self)
        self.view.addSubview(vc.view)
//        vc.modalTransitionStyle = .crossDissolve
//        vc.modalPresentationStyle = .overCurrentContext
//        self.present(vc, animated: true, completion: nil)
    }
    
    
}

extension ReorderVc:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let spacer = tableView.reorder.spacerCell(for: indexPath) {
            return spacer
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "assignedCell") as! assignedCell
        cell.lblName.text = dataArray[indexPath.row].jobName ?? ""
        cell.lblDateTime.text = dataArray[indexPath.row].assigndate ?? ""
        cell.lblJobNo.text = "# \(dataArray[indexPath.row].po ?? "")"
        cell.lblAddress.text = dataArray[indexPath.row].location ?? ""
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 75
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }

    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject = self.dataArray[sourceIndexPath.row]
        dataArray.remove(at: sourceIndexPath.row)
        dataArray.insert(movedObject, at: destinationIndexPath.row)
    }
    
}

extension ReorderVc: TableViewReorderDelegate {
    func tableView(_ tableView: UITableView, reorderRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject = self.dataArray[sourceIndexPath.row]
        dataArray.remove(at: sourceIndexPath.row)
        dataArray.insert(movedObject, at: destinationIndexPath.row)
    }
}
