
//
//  ViewCommentsVC.swift
//  Crewtok
//
//  Created by Rishabh Arora on 28/08/19.
//  Copyright © 2019 Rishabh Arora. All rights reserved.
//

import UIKit
import SwiftyJSON
import IQKeyboardManagerSwift

class ViewCommentsTVC:UITableViewCell{
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgComment: UIImageViewCustomClass!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
}

class ViewCommentsVC: BaseViewController {
    
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var txtComment: UITextFieldCustomClass!
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    
    var jobData = JobsData()
    var chatArray = [JSON]()
    var detailVC = JobDetailVC()

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        getData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    
    @objc func  keyboardWillShow(notification : NSNotification)
    {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.bottomConst.constant = keyboardFrame.size.height - 75
        })
        self.tableViewScrollToBottom(animated: false)
    }
    
    @objc func  keyboardWillHide(notification : NSNotification)
    {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.bottomConst.constant = 20
        })
    }
    
    func tableViewScrollToBottom(animated: Bool)
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300))
        {
            let numberOfSections = self.tblVw.numberOfSections
            if numberOfSections > 0
            {
                let numberOfRows = self.tblVw.numberOfRows(inSection: numberOfSections-1)
                
                if numberOfRows > 0
                {
                    let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                    self.tblVw.scrollToRow(at: indexPath, at: .bottom, animated: animated)
                }
            }
        }
    }
    
    @IBAction func btnCrossAction(_ sender: UIButton) {
        detailVC.getData()
        self.dismissBtnAction()
    }
    
    @IBAction func btnCommentAction(_ sender: UIButton) {
        if txtComment.isEmpty{
            self.showAlert(message: "Please enter comment", type: .error)
        }
        else{
            apiCommentData()
        }
    }
    
}

extension ViewCommentsVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ViewCommentsTVC
        let data = chatArray[indexPath.row]
        cell.lblName.text = data["name"].stringValue
        cell.lblDate.text = data["craeteddate"].stringValue
        cell.lblAddress.text = data["body"].stringValue.parseHtmlString
        cell.imgComment.sd_setImage(with: URL(string: "\(IMG_URL)\(data["image"].stringValue.replacingOccurrences(of: "~", with: ""))")!, placeholderImage: #imageLiteral(resourceName: "dummy_profile"))
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 75
        return UITableView.automaticDimension
    }
    
}

extension ViewCommentsVC{
    
    func getData(){
        
        if (reachability?.isReachable)! {
            applicationDelegate.showActivityIndicator()
            
            let url = "\(Api.VIEW_COMMENT.rawValue)?jobid=\(jobData.jobId!)"
            
            AlamoFireWrapperNetwork.sharedInstance.GetDataAlamofire(url: url, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    
                    self.chatArray = responseDict!["data"].arrayValue
                    self.reloadData()
                    
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
    func apiCommentData(){
        
        if (reachability?.isReachable)! {
            applicationDelegate.showActivityIndicator()
            
            let parameterDict = ["userid":profileData?.sessionid ?? "","jobid":jobData.jobId!,"body":txtComment.text!,"file":""] as [String : Any]
            
            AlamoFireWrapperNetwork.sharedInstance.PostDataAlamofire(parameterDict as [String : Any],url: Api.ADD_COMMENT.rawValue, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    self.txtComment.text = ""
                    self.chatArray.append(responseDict!["data"])
                    self.reloadData()
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
    func reloadData(){
        self.tblVw.reloadData()
        if chatArray.count > 0{
            self.tblVw.scrollToRow(at: IndexPath(row: chatArray.count - 1, section: 0), at: .none, animated: false)
        }
    }
    
}
