//
//  ViewContactVC.swift
//  Crewtok
//
//  Created by Rishabh Arora on 28/08/19.
//  Copyright © 2019 Rishabh Arora. All rights reserved.
//

import UIKit
import MessageUI

class ViewContactVC: BaseViewController {
    
    @IBOutlet weak var lblJobLocation: UILabel!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblPrimaryContact: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    
    var jobData = JobsData()
    var phoneNo = ""
    var emailStr = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getData()
    }
    
    @IBAction func btnCrossAction(_ sender: UIButton) {
        self.dismissBtnAction()
    }
    
    @IBAction func btnCallAction(_ sender: UIButton) {
        
        guard let number = URL(string: "tel://\(phoneNo)") else { return }
        if UIApplication.shared.canOpenURL(number){
            UIApplication.shared.open(number)
        } else {
            self.showAlert(message: "Phone number not avilable", type: .error)
        }
    }
    
    @IBAction func btnMailAction(_ sender: UIButton) {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients(["\(emailStr)"])
        mailComposerVC.setSubject("Crewtok")
        mailComposerVC.setMessageBody("", isHTML: false)
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposerVC, animated: true, completion: nil)
        } else {
            self.showAlert(message: "E-mail not available", type: .error)
        }
    }
    
}

extension ViewContactVC:MFMailComposeViewControllerDelegate{
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}

extension ViewContactVC{
    
    func getData(){
        
        if (reachability?.isReachable)! {
            applicationDelegate.showActivityIndicator()
            
            let url = "\(Api.JOB_CONTACT.rawValue)?jobid=\(jobData.jobId!)"
            
            AlamoFireWrapperNetwork.sharedInstance.GetDataAlamofire(url: url, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    
                    self.lblPrimaryContact.text = responseDict!["data"]["customername"].stringValue
                    self.lblCompany.text = responseDict!["data"]["companyname"].stringValue
                    self.lblJobLocation.text = responseDict!["data"]["jobaddress"].stringValue.parseHtmlString
                    self.lblPhone.text = responseDict!["data"]["customerphone"].stringValue
                    self.lblEmail.text = responseDict!["data"]["customeremail"].stringValue
                    self.phoneNo = responseDict!["data"]["customerphone"].stringValue
                    self.emailStr = responseDict!["data"]["customeremail"].stringValue
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
}
