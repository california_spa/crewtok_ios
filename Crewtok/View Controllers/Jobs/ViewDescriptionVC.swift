
//
//  ViewDescriptionVC.swift
//  Crewtok
//
//  Created by Rishabh Arora on 28/08/19.
//  Copyright © 2019 Rishabh Arora. All rights reserved.
//

import UIKit

class ViewDescriptionVC: BaseViewController {
    
    @IBOutlet weak var lblDesc: UITextView!
    var jobData = JobsData()

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        getData()
    }
    
    @IBAction func btnCrossAction(_ sender: UIButton) {
        self.dismissBtnAction()
    }
    

}

extension ViewDescriptionVC{
    
    func getData(){
        
        if (reachability?.isReachable)! {
            applicationDelegate.showActivityIndicator()
            
            let url = "\(Api.JOB_DESCRIPTION.rawValue)?jobid=\(jobData.jobId!)"
            
            AlamoFireWrapperNetwork.sharedInstance.GetDataAlamofire(url: url, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    
                    self.lblDesc.text = responseDict!["data"].stringValue.parseHtmlString
                    
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
}
