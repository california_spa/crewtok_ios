//
//  LoginVC.swift
//  CrewtokApp
//
//  Created by cql on 17/07/19.
//  Copyright © 2019 manoj. All rights reserved.
//

import UIKit

class LoginVC: BaseViewController {
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if loginEmail != nil{
            txtEmail.text = loginEmail!
            txtPassword.text = loginPassword!
        }
        
        //        txtEmail.text = "dev1@email.com"
        //        txtPassword.text = "Fullspeed1"
    }
    
    
    @IBAction func SignInBtn(_ sender: Any) {
        if txtEmail.isEmpty{
            self.showAlert(message: "Please enter email", type: .error)
        }
        else if txtPassword.isEmpty{
            self.showAlert(message: "Please enter password", type: .error)
        }
        else{
            apiLogin()
        }
    }
    
    @IBAction func btnSupportAction(_ sender: Any) {
        self.openUrl(SUPPORT_URL)
    }
    
    @IBAction func btnForgetAction(_ sender: Any) {
        self.openUrl(FORGOT_URL)
    }
    
}


extension LoginVC{
    
    func apiLogin(){
        if (reachability?.isReachable)! {
            applicationDelegate.showActivityIndicator()
            
            let headerDict = ["Accept":"application/json"]
            let parameterDict = ["email":txtEmail.text!,"password":txtPassword.text!,"device_id":applicationDelegate.deviceTokenString]
            AlamoFireWrapperNetwork.sharedInstance.PostDataAlamofire(parameterDict as [String : Any],url: Api.LOGIN.rawValue,headers: headerDict, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    loginEmail = self.txtEmail.text!
                    loginPassword = self.txtPassword.text!
                    profileData = try! JSONDecoder().decode(ProfileModel.self, from: data!)
                    loginData = data!
                    self.apiUpdateLocation()
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
    func apiUpdateLocation(){
        if (reachability?.isReachable)! {
            applicationDelegate.showActivityIndicator()
            
            let headerDict = ["Accept":"application/json"]
            let parameterDict = ["userid":profileData?.sessionid ?? "","location":sourceLoc!]
            
            AlamoFireWrapperNetwork.sharedInstance.PostDataAlamofire(parameterDict as [String : Any],url: Api.UPDATE_LIVE_STATUS.rawValue,headers: headerDict, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    self.goToHomeVc()
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
}
