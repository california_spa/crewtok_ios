
//
//  ProfileDetailVC.swift
//  Crewtok
//
//  Created by Rishabh Arora on 24/10/19.
//  Copyright © 2019 Rishabh Arora. All rights reserved.
//

import UIKit

class ProfileDetailVC: BaseViewController {
    
    @IBOutlet weak var imgProfile: UIImageViewCustomClass!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblAddress: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        let imgStr = "\(IMG_URL)\((profileData?.data?.image ?? "").replacingOccurrences(of: "~", with: ""))"
        imgProfile.sd_setImage(with: URL(string: imgStr), placeholderImage: #imageLiteral(resourceName: "dummy_profile"))
        lblName.text = profileData?.data?.name ?? ""
        lblCompany.text = profileData?.data?.company ?? ""
        lblEmail.text = profileData?.data?.Email ?? ""
        lblPhone.text = profileData?.data?.phone ?? ""
        lblAddress.text = profileData?.data?.address ?? ""
        
    }
    

}
