
//
//  ProfileVC.swift
//  Crewtok
//
//  Created by cqli on 10/09/19.
//  Copyright © 2019 Rishabh Arora. All rights reserved.
//

import UIKit
import SDWebImage

class ProfileVC: BaseViewController {
    
    @IBOutlet weak var imgProfile: UIImageViewCustomClass!
    @IBOutlet weak var lblClockedIn: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTotalAssigned: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        let imgStr = "\(IMG_URL)\((profileData?.data?.image ?? "").replacingOccurrences(of: "~", with: ""))"
        imgProfile.sd_setImage(with: URL(string: imgStr), placeholderImage: #imageLiteral(resourceName: "dummy_profile"))
        lblName.text = profileData?.data?.name ?? ""
        lblClockedIn.text = applicationDelegate.lastClockedMessage
        lblTotalAssigned.text = "\(applicationDelegate.assignjobs) assigned"

    }
    
    @IBAction func btnDismissAction(_ sender: Any) {
        self.dismissBtnAction()
    }
    
    @IBAction func btnActions(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            goToProfileDetailVc()
        case 1:
            applicationDelegate.isTimeClock = true
            self.goToHomeVc()
        case 2:
            self.goToAssignedVc()
        case 3:
            self.goToContactVC()
        case 4:
            self.goToAssigmentVc()
        default:
            self.logOut()
        }
        self.dismissBtnAction()
        
    }
    

}
