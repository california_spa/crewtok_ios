//
//  SidemenuVC.swift
//  LearndayApp
//
//  Created by vinay on 11/12/18.
//  Copyright © 2018 Manoj. All rights reserved.
//

import UIKit
import SwiftMessageBar
import LGSideMenuController

class sidemenuCell: UITableViewCell {
    @IBOutlet weak var Namelbl: UILabel!
}

class SidemenuVC: BaseViewController,UITableViewDelegate,UITableViewDataSource{
    
    var StudentArray = ["Home","Time clock","Job","Chat","Equipment"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func signOutBtn(_ sender: UIButton) {
          logOut()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return StudentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sidemenuCell") as! sidemenuCell
        cell.Namelbl.text = StudentArray[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let mainViewController = sideMenuController!
        let navigationController = mainViewController.rootViewController as! UINavigationController
        var mainVc = UIViewController()
        
        if indexPath.row  == 0 {
            applicationDelegate.isTimeClock = false
            mainVc = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        }
        else if indexPath.row  == 1 {
            applicationDelegate.isTimeClock = true
            mainVc = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        }
        else if indexPath.row  == 2 {
            mainVc = mainStoryboard.instantiateViewController(withIdentifier: "AssignedVc") as! AssignedVc
        }
        else if indexPath.row  == 3 {
            mainVc = mainStoryboard.instantiateViewController(withIdentifier: "ContactVC") as! ContactVC
        }
        else if indexPath.row  == 4 {
            mainVc = mainStoryboard.instantiateViewController(withIdentifier: "AssigmentVc") as! AssigmentVc
        }
        
        navigationController.setViewControllers([mainVc], animated: false)
        mainViewController.hideLeftViewAnimated()
        
    }
    
    
}




