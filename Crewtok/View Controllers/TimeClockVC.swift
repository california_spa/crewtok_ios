//
//  TimeClockVC.swift
//  Crewtok
//
//  Created by Rishabh Arora on 24/08/19.
//  Copyright © 2019 Rishabh Arora. All rights reserved.
//

import UIKit

class TimeClockVC: BaseViewController {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLastClocked: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var btnClockIn: UIButtonCustomClass!
    
    var isClockin = false

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getData()
    }
    
    @IBAction func btnCrossAction(_ sender: Any) {
        self.dismissBtnAction()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismissBtnAction()
    }
    
    @IBAction func btnClockInAction(_ sender: Any) {
        self.updateStatusApi()
    }
    
    
    
}


extension TimeClockVC{
    
    func getData(){
        
        if (reachability?.isReachable)! {
            applicationDelegate.showActivityIndicator()
            
            let parameterDict = ["userid":profileData?.sessionid ?? ""] as [String : Any]
            AlamoFireWrapperNetwork.sharedInstance.PostDataAlamofire(parameterDict as [String : Any],url: Api.CLOCK_INOUT.rawValue, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    let currentDateTime = self.getCurrentDateTime()
                    
                    self.lblName.text = responseDict!["data"]["name"].stringValue
                    self.lblLastClocked.text = responseDict!["data"]["lastmessage"].stringValue
                    
                    let attrTime = NSMutableAttributedString()
                    let attrDate = NSMutableAttributedString()
                    let attrLoc = NSMutableAttributedString()
                    
                    if UIDevice.isPhone{
                     let heading = NSAttributedString(string: "Current time: ", attributes: [NSAttributedString.Key.font: UIFont(name: "Poppins-SemiBold", size: 17.0)!])
                        let value = NSAttributedString(string: "\(currentDateTime.1)", attributes: [NSAttributedString.Key.font: UIFont(name: "Poppins-Regular", size: 17.0)!])
                        attrTime.append(heading)
                        attrTime.append(value)
                    }
                    else{
                        let heading = NSAttributedString(string: "Current time: ", attributes: [NSAttributedString.Key.font: UIFont(name: "Poppins-SemiBold", size: 20.0)!])
                        let value = NSAttributedString(string: "\(currentDateTime.1)", attributes: [NSAttributedString.Key.font: UIFont(name: "Poppins-Regular", size: 20.0)!])
                        attrTime.append(heading)
                        attrTime.append(value)
                    }
                    self.lblTime.attributedText = attrTime
                    
                    
                    if UIDevice.isPhone{
                        let heading = NSAttributedString(string: "Date: ", attributes: [NSAttributedString.Key.font: UIFont(name: "Poppins-SemiBold", size: 17.0)!])
                        let value = NSAttributedString(string: "\(currentDateTime.0)", attributes: [NSAttributedString.Key.font: UIFont(name: "Poppins-Regular", size: 17.0)!])
                        attrDate.append(heading)
                        attrDate.append(value)
                    }
                    else{
                        let heading = NSAttributedString(string: "Date: ", attributes: [NSAttributedString.Key.font: UIFont(name: "Poppins-SemiBold", size: 20.0)!])
                        let value = NSAttributedString(string: "\(currentDateTime.0)", attributes: [NSAttributedString.Key.font: UIFont(name: "Poppins-Regular", size: 20.0)!])
                        attrDate.append(heading)
                        attrDate.append(value)
                    }
                    self.lblDate.attributedText = attrDate
                    
                    if UIDevice.isPhone{
                        let heading = NSAttributedString(string: "Location: ", attributes: [NSAttributedString.Key.font: UIFont(name: "Poppins-SemiBold", size: 17.0)!])
                        let value = NSAttributedString(string: "\(sourceLoc!)", attributes: [NSAttributedString.Key.font: UIFont(name: "Poppins-Regular", size: 17.0)!])
                        attrLoc.append(heading)
                        attrLoc.append(value)
                    }
                    else{
                        let heading = NSAttributedString(string: "Location: ", attributes: [NSAttributedString.Key.font: UIFont(name: "Poppins-SemiBold", size: 20.0)!])
                        let value = NSAttributedString(string:  "\(sourceLoc!)", attributes: [NSAttributedString.Key.font: UIFont(name: "Poppins-Regular", size: 20.0)!])
                        attrLoc.append(heading)
                        attrLoc.append(value)
                    }
                    self.lblLocation.attributedText = attrLoc
                    
                    self.isClockin = responseDict!["data"]["isclockin"].boolValue
                    if !self.isClockin{
                      self.btnClockIn.setTitle("Clock Out", for: .normal)
                    }
                    else{
                      self.btnClockIn.setTitle("Clock In", for: .normal)
                    }
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
    func updateStatusApi(){
        if (reachability?.isReachable)! {
            applicationDelegate.showActivityIndicator()
            
            let currentDateTime = self.getCurrentDateTime()
            let timeStr = "\(currentDateTime.1)"
            
            let parameterDict = ["location":sourceLoc!,"UserId":profileData?.sessionid ?? "","time":timeStr] as [String : Any]
            AlamoFireWrapperNetwork.sharedInstance.PostDataAlamofire(parameterDict as [String : Any],url: Api.UPDATE_CLOCK_INOUT.rawValue, success: { (responseDict,data) in
                debugPrint(responseDict ?? "No Value")
                let message = responseDict![KError].stringValue
                if message == "" {
                    
                    self.dismissBtnAction()
                    
                }
                else{
                    self.showAlert(message: message, type: .error)
                }
            }, failure: {(error) in
                self.showAlert(message: error?.localizedDescription ?? "", type: .error)
            })
        }
        else{
            self.showAlert(message: kInternetConnection, type: .error)
        }
        
    }
    
}
