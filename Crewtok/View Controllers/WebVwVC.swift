
//
//  WebVwVC.swift
//  Crewtok
//
//  Created by Rishabh Arora on 14/10/19.
//  Copyright © 2019 Rishabh Arora. All rights reserved.
//

import UIKit
import WebKit

class WebVwVC: UIViewController,UIWebViewDelegate {
    
    @IBOutlet weak var webVw: UIWebView!
    var urlStr = String()

    override func viewDidLoad() {
        super.viewDidLoad()

        webVw.delegate = self
        let request = URLRequest(url: URL(string: urlStr)!)
        webVw.loadRequest(request)
        
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        applicationDelegate.showActivityIndicator()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        applicationDelegate.hideActivityIndicator()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        applicationDelegate.hideActivityIndicator()
    }
    

}
