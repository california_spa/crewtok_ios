//
//  welcomeVc.swift
//  CrewtokApp
//
//  Created by cql on 17/07/19.
//  Copyright © 2019 manoj. All rights reserved.
//

import UIKit

class welcomeVc: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func SignInBtn(_ sender: Any) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnRequestAccountAction(_ sender: Any) {
        self.openUrl(REGISTER_URL)
    }
    
    @IBAction func btnSupportAction(_ sender: Any) {
        self.openUrl(SUPPORT_URL)
    }
    
    
}
